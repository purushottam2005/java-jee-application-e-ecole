/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.ecdf.model;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author stagiaire
 */
@Slf4j
public class PersonneTest {

    @Test
    public void testPersonneDisplayType() {
        /*Adresse adresse1 = new Adresse("rue1",12345,"lyon");
         LoginInfos login1 =new LoginInfos("mdp1","login2");
         Personne personne =new Admin("nom4","prenom4","eMail1","photo1",adresse1,login1);
        
         Adresse adresse2 = new Adresse("rue2",12346,"paris");
         LoginInfos login2 =new LoginInfos("mdp2","login3");
         Personne personne2 =new Admin("nom4","prenom4","eMail2","photo2",adresse2,login2);
        
         assertEquals("Personne", personne.returnType());
         assertEquals("Admin", personne2.returnType());
        
         Adresse adresse3 = new Adresse("rue3",12347,"toulouse");
         LoginInfos login3 =new LoginInfos("mdp3","login4");
         Personne personne3 =new Enseignant("nom4","prenom4","eMail4","photo5",adresse3,login3);
        
         assertEquals("Personne", personne.returnType());
         assertEquals("Admin", personne2.returnType());
         assertEquals("Enseignant", personne3.returnType());
        
        
         Adresse adresse4 = new Adresse("rue4",12348,"toulon");
         LoginInfos login4 =new LoginInfos("mdp4","login5");
         Personne personne4 =new Enseignant("nom3","prenom3","eMail5","photo6",adresse4,login4);
         Personne personne5 =new Etudiant("nom4","prenom4","eMail5","photo6",adresse4,login4,new Date(2013/10/05));
        
         assertEquals("Personne", personne.returnType());
         assertEquals("Admin", personne2.returnType());
         assertEquals("Enseignant", personne3.returnType());
         assertEquals("Enseignant", personne4.returnType());
         assertEquals("Etudiant", personne5.returnType());
        
         Map<String,Personne> map = new HashMap<String, Personne>();
         map.put(personne2.returnKey(), personne2);
         map.put(personne3.returnKey(), personne3);
         map.put(personne4.returnKey(), personne4);
         map.put(personne5.returnKey(), personne5);*/

        Adresse adresse1 = new Adresse("rue2", 12645, "toulon");
        LoginInfos login1 = new LoginInfos("mdp1", "login2");
        Personne personne = new Admin("nom4", "prenom4", "eMail1", "photo1", adresse1, login1);

        Adresse adresse2 = new Adresse("rue1", 12345, "lyon");
        LoginInfos login2 = new LoginInfos("mdp1", "login2");
        Personne personne2 = new Admin("nom4", "prenom4", "eMail1", "photo1", adresse2, login2);


        //assertEquals(1, map.size());


        log.info(personne.toString());
        //assertEquals("nom: nom4 prenom: prenom4 email: eMail1 photo: photo1 adresse: rue2 12645 toulon  LoginInfos mdp1 login2", personne.toString());

        log.info(personne2.toString());
        //assertEquals("nom: nom4 prenom: prenom4 email: eMail1 photo: photo1 adresse: rue1 12345 lyon  LoginInfos mdp1 login2", personne2.toString());

    }
}
package com.sqli.ecdf.model;

import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

public class EnseignantTest {

    @Test
    public void testCreateAbscence() {
        Cours cours = new Cours(new Date(2013 / 12 / 05), 50, "jee");
        Etudiant etudiant = new Etudiant("nom1", "prenom1", "email1", "photo1", new Adresse("ri", 12540, "toulon"),
                new LoginInfos("mdp1", "login1"), new Date(2013 / 12 / 05));
        Absence abs = new Absence("motif1", cours, etudiant);
        assertEquals("nom1", abs.getEtudiant().getNom());
        assertEquals("prenom1", abs.getEtudiant().getPrenom());
    }
}
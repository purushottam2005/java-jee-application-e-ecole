package com.sqli.ecdf.model;

import com.sqli.ecdf.model.exception.KeyAlreadyThereException;



public class Admin extends Personne{

    
    public Admin(String nom, String prenom, String email, String photo, Adresse adresse, LoginInfos login) {
        super(nom, prenom, email, photo, adresse, login);
    }
    
    public void addPersonne(Personne personne) throws KeyAlreadyThereException{
    }

    @Override
    public String returnType() {
        return "Admin";
    }
}

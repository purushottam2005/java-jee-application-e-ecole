package com.sqli.ecdf.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Adresse {

    String rue;
    int codePostal;
    String ville;
}

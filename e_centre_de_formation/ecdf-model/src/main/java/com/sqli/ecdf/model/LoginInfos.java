package com.sqli.ecdf.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class LoginInfos {

    String motDePasse;
    String login;
}

package com.sqli.ecdf.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class Enseigne {

    private Promotion promotion;
    private Matiere matiere;

  
}

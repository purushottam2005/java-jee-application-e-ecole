package com.sqli.ecdf.services;

import com.sqli.ecdf.model.Absence;
import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.exception.KeyAlreadyThereException;
import com.sqli.ecdf.model.exception.KeyNotThereException;
import com.sqli.ecdf.model.Matiere;
import com.sqli.ecdf.model.Personne;
import com.sqli.ecdf.model.Promotion;

public interface InterfaceAdmin {

    void addPersonne(Personne personne)throws KeyAlreadyThereException;

    void removePersonne(String keyPersonne) throws KeyNotThereException;

    void addMatiere(String nom, Matiere matiere) throws KeyAlreadyThereException;

    void removeMatiere(String nom) throws KeyNotThereException;

    Matiere getMatiere(String nom) throws KeyNotThereException;

    void addPromotion(String nomPromotion, Promotion promotion) throws KeyAlreadyThereException;

    void removePromotion(String nompromotion)throws KeyNotThereException;

    void addCours(Cours cours)throws KeyAlreadyThereException;

    void removeCours(String description)throws KeyNotThereException;

    void addAbsence(String absenceName, Absence absence) throws KeyAlreadyThereException;

    void removeAbsence(String motif) throws KeyNotThereException;
}

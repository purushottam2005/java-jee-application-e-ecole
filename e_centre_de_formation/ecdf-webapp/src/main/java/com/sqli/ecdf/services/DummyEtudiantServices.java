package com.sqli.ecdf.services;

import com.sqli.ecdf.dao.AbsenceDAO;
import com.sqli.ecdf.dao.CoursContentsDAO;
import com.sqli.ecdf.dao.CoursDAO;
import com.sqli.ecdf.dao.PersonneDAO;
import com.sqli.ecdf.model.Absence;
import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.Matiere;
import com.sqli.ecdf.model.Personne;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.Setter;

public class DummyEtudiantServices implements EtudiantServices {

    @Setter
    private AbsenceDAO absenceDAO;
    @Setter
    private CoursDAO coursDAO;
    @Setter
    private CoursContentsDAO coursContentsDAO;
    @Setter
    private PersonneDAO personneDAO;

    
   
    @Override
    public List<Cours> getAllExistingCours() {
        return coursDAO.getAllExistingCours();
    }
    @Override
    public Set<Cours> getAllCours(){
        return coursDAO.loadAgainAllCours();
    }
    
    @Override
    public List<Absence> getAllExistingAbsence() {
        return absenceDAO.getAllExistingAbsence();
    }

    @Override
    public Set<Absence> getAllAbsence() {
        return absenceDAO.getAllAbsences();
    }
    
    @Override
     public List<Matiere> getAllExistingMatiereContentCours() {
        return coursContentsDAO.getAllExistingMatiereContentCours();
    }
    
     public Map<String, Matiere> getAllMatiereListCours(){
         return coursContentsDAO.getAllMatiereListCours();
     }
    
    @Override
     public Map<String, Matiere> getOneMatiereListCours(String nomMatiere){
        return coursContentsDAO.getOneMatiereListCours(nomMatiere);
     }
    
    @Override
    public Map<String, Absence> getAllAbsenceOneUser(String userName){
        return absenceDAO.getAllAbsenceOneUser(userName);
    }
    
    @Override
    public Map<String, Absence> getAllAbsenceUnEtudiantDAO(String nom, String prenom){
        return absenceDAO.getAllAbsenceUnEtudiantDAO(nom, prenom);
    } 
    @Override
    public Map<String, Personne> returnOnePersonneEtudiantDAO(String loginUser){
        return personneDAO.returnOnePersonneEtudiantDAO(loginUser);
    }

    
}

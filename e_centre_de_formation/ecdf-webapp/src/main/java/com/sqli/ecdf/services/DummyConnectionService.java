package com.sqli.ecdf.services;

import com.sqli.ecdf.dao.PersonneDAO;
import com.sqli.ecdf.dao.UserDAO;
import com.sqli.ecdf.model.LoginInfos;
import com.sqli.ecdf.model.Personne;
import java.util.List;
import java.util.Map;
import lombok.Setter;

public class DummyConnectionService implements ConnectionService {

    @Setter
    private UserDAO userDAO;
    
    @Setter
    private PersonneDAO personneDAO;

    @Override
    public boolean isIdentifier(String loginForm, String passwordForm) {
        return userDAO.isIdentifier(loginForm, passwordForm);
    }

    @Override
    public List<LoginInfos> userList() {
        return userDAO.userList();
    }
    
    @Override
     public Map<String, Personne> returnOnePersonneEtudiantDAO(String loginUser){
         return personneDAO.returnOnePersonneEtudiantDAO(loginUser);
     }

}

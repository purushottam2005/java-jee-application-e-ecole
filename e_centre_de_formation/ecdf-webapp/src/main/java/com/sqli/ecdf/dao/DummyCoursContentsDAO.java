package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.Matiere;
import com.sqli.ecdf.model.exception.KeyAlreadyThereException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DummyCoursContentsDAO implements CoursContentsDAO {

    public static Date parseDate(String date) throws ParseException {
        Date result = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat();
            result = formatter.parse(date);
            return result;
        } catch (RuntimeException e) {
            return null;
        }
    }

    @Override
    public Map<String, Matiere> getAllMatiereListCours() {
        Map<String, Matiere> matieres = new HashMap<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        try {
            Date date1 = df.parse("20/12/2012");
            Date date2 = df.parse("01/08/2012");
            Date date3 = df.parse("20/09/2012");
            Date date4 = df.parse("15/12/2012");
            Date date5 = df.parse("05/02/2013");
            Cours cours1 = new Cours(date1, 420, "JAVA les bases");
            Cours cours2 = new Cours(date2, 380, "HTML les bases");
            Cours cours3 = new Cours(date3, 380, "MAVEN");
            Cours cours4 = new Cours(date4, 210, "ANDROID");
            Cours cours5 = new Cours(date5, 80, "CSS3");

            Matiere matiere1 = new Matiere("JAVA JEE");
            matiere1.addCours(cours1);
            matiere1.addCours(cours3);

            Matiere matiere2 = new Matiere("HTML");
            matiere2.addCours(cours2);
            matiere2.addCours(cours5);

            matieres.put(matiere1.getNom(), matiere1);
            matieres.put(matiere2.getNom(), matiere2);

        } catch (ParseException ex) {
            Logger.getLogger(DummyCoursDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (KeyAlreadyThereException ex) {
            Logger.getLogger(DummyCoursContentsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return matieres;

    }

    @Override
    public List<Matiere> getAllExistingMatiereContentCours() {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        List<Matiere> listMatiere = new ArrayList();

        try {
            Date date1 = df.parse("20/12/2012");
            Date date2 = df.parse("01/08/2012");
            Date date3 = df.parse("20/09/2012");
            Date date4 = df.parse("15/12/2012");
            Date date5 = df.parse("05/02/2013");
            Cours cours1 = new Cours(date1, 420, "JAVA les bases");
            Cours cours2 = new Cours(date2, 380, "HTML les bases");
            Cours cours3 = new Cours(date3, 380, "MAVEN");
            Cours cours4 = new Cours(date4, 210, "ANDROID");
            Cours cours5 = new Cours(date5, 80, "CSS3");

            Matiere matiere1 = new Matiere("JAVA JEE");
            matiere1.addCours(cours1);
            matiere1.addCours(cours3);

            Matiere matiere2 = new Matiere("HTML");
            matiere2.addCours(cours2);
            matiere2.addCours(cours5);

            listMatiere.add(matiere1);
            listMatiere.add(matiere2);

        } catch (ParseException ex) {
            Logger.getLogger(DummyCoursDAO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (KeyAlreadyThereException ex) {
            Logger.getLogger(DummyCoursContentsDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listMatiere;

    }

    @Override
    public Map<String, Matiere> getOneMatiereListCours(String nom) {
        Map<String, Matiere> allMatiere = getAllMatiereListCours();
        Map<String, Matiere> onMatiereAllCours = new HashMap<>();

        for (Matiere matiere : allMatiere.values()) {
            if (matiere.getNom().equals(nom)) {
                onMatiereAllCours.put(matiere.getNom(), matiere);
            }
        }
        return onMatiereAllCours;
    }
}

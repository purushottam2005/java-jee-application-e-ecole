package com.sqli.ecdf.services;

import com.sqli.ecdf.model.Absence;
import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.Etudiant;
import com.sqli.ecdf.model.Matiere;
import com.sqli.ecdf.model.Personne;
import java.util.List;
import java.util.Map;

public interface EnseignantServices {

    public List<Matiere> getAllExistingMatiereContentCours();

    public Map<String, Personne> returnPersonneEtudiantDAO();
    
    public Map<String, Personne> returnAlonePersonneEtudiantDAO(String nom, String prenom);

    public Map<String, Absence> getAllAbsenceEtudiantDAO();

    public Map<String, Absence> getAllAbsenceUnEtudiantDAO(String nom, String prenom);

    void addCoursContents(Cours cours, String description);

    void removeCoursContents(Cours cours, String description);

    void addAbsence(String motif, Cours cours, Etudiant etudiant);

    void removeAbsence(String motif, Cours cours, Etudiant etudiant);
}

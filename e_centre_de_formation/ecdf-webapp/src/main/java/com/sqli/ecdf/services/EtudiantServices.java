package com.sqli.ecdf.services;

import com.sqli.ecdf.model.Absence;
import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.Matiere;
import com.sqli.ecdf.model.Personne;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface EtudiantServices {

    public Map<String, Personne> returnOnePersonneEtudiantDAO(String loginUser);
    
    public List<Cours> getAllExistingCours();

    public List<Matiere> getAllExistingMatiereContentCours();

    public Map<String, Matiere> getOneMatiereListCours(String nomMatiere);
    
    public Set<Cours> getAllCours();

    public List<Absence> getAllExistingAbsence();

    public Set<Absence> getAllAbsence();

    public Map<String, Absence> getAllAbsenceOneUser(String userName);

    public Map<String, Absence> getAllAbsenceUnEtudiantDAO(String nom, String prenom);
}

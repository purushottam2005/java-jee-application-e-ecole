package com.sqli.ecdf.webapp.controleur;

import com.sqli.ecdf.dao.DummyPersonneDAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.Setter;

@WebServlet(name = "CrudPersonneServlet", urlPatterns = {"/CrudPersonne"})
public class CrudPersonneServlet extends HttpServlet {

    @Setter
    private static DummyPersonneDAO personneService;

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nom = req.getParameter("nom");
        String prenom = req.getParameter("prenom");
        req.setAttribute("alonePersonne", personneService.returnAlonePersonneDAO(nom, prenom));
        req.setAttribute("listeDePersonne", personneService.returnPersonneDAO());

        String viewName = "/WEB-INF/views/listePersonne.jsp";
        displayJspPage(viewName, req, resp);
    }

    public void displayJspPage(String pageLocation, ServletRequest req, ServletResponse res) throws ServletException, IOException {
        RequestDispatcher disp = this.getServletContext().getRequestDispatcher(pageLocation);
        disp.forward(req, res);

    }
}

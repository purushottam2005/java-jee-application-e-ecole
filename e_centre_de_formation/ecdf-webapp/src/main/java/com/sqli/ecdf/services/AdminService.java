package com.sqli.ecdf.services;

import com.sqli.ecdf.dao.ManagedSchool;
import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.Etudiant;
import com.sqli.ecdf.model.KeyAlreadyThereException;
import com.sqli.ecdf.model.KeyNotThereException;
import com.sqli.ecdf.model.LoginInfos;
import com.sqli.ecdf.model.Matiere;
import com.sqli.ecdf.model.Personne;
import com.sqli.ecdf.model.Promotion;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AdminService implements InterfaceAdmin {
    @Getter
    private ManagedSchool ecole;
    
    public AdminService(ManagedSchool ecole){
        this.ecole = ecole;
    
}

   
    @Override
    public void addPersonne(Personne personne, LoginInfos login) {
        try {
            ecole.addPersonneAndLogin(personne, login);
        } catch (KeyAlreadyThereException ex) {
            log.warn(AdminService.class.getName());
        }
    }

    @Override
    public String returnType(Personne personne){
        
      return(personne.returnType());  
    }
    
    @Override
    public void removePersonne(Personne personne) {
        try {
            ecole.removePersonne(personne);
        } catch (KeyNotThereException ex) {
            log.warn(AdminService.class.getName());
        }
    }

    @Override
    public void addMatiere(String nom, Matiere  matiere) {
        ecole.addMatiere(nom, matiere);
    }

    @Override
    public void removeMatiere(String nom) {
        ecole.removeMatiere(nom);
    }

    @Override
    public String getMatiere(Matiere matiere) {
        return matiere.getNom();
    }

    @Override
    public void addPromotion(Promotion promotion) {
        ecole.addPromotion(promotion);
    }

    @Override
    public void removePromotion(Promotion promotion) {
        ecole.removePromotion(promotion);
    }


    @Override
    public void addCours(Cours cours) {
      ecole.addCours(cours);
    }
    @Override
    public void removeCours(String description) {
        ecole.removeCours(description);
    }

    @Override
    public void addAbsence(String motif, Cours cours, Etudiant etudiant) {
        ecole.addAbsence(motif, cours, etudiant);
    }

    @Override
    public void removeAbsence(String motif, Cours cours, Etudiant etudiant) {
        ecole.removeAbsence(motif, cours, etudiant);
    }
}

    


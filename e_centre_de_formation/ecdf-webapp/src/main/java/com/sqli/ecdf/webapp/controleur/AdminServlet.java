/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.ecdf.webapp.controleur;

import com.sqli.ecdf.dao.DummyCoursDAO;
import com.sqli.ecdf.dao.DummyPersonneDAO;
import com.sqli.ecdf.dao.DummyPromotionDAO;
import com.sqli.ecdf.dao.DummyUserDAO;
import com.sqli.ecdf.services.DummyAdminServices;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebServlet(name = "AdminServlet", urlPatterns = {"/ListeAdmin"})
public class AdminServlet extends HttpServlet {

    @Setter
    private static DummyPersonneDAO personneService;
    @Setter
    private static DummyPromotionDAO promotionService;
    @Setter
    private static DummyCoursDAO coursService;
    @Setter
    private static DummyUserDAO userService;
    @Setter
    private static DummyAdminServices adminServices;

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String viewName;
        String switchAction = req.getParameter("nav");

        if ("displayAllPerson".equals(switchAction)) {
            req.setAttribute("listeDePersonne", adminServices.returnPersonneDAO());
            viewName = "/WEB-INF/views/listePersonne.jsp";
        } else {
            if ("addPerson".equals(switchAction)) {

                req.setAttribute("listeDePersonne", "");
                viewName = "/WEB-INF/views/addPersonne.jsp";
            } else {
                if ("displayAllPromotion".equals(switchAction)) {
                    String lsNomFichier = getServletContext().getRealPath("/resources/promotions.csv");
                    req.setAttribute("listeDePromotion", promotionService.loadAllPromotion(lsNomFichier));
                    viewName = "/WEB-INF/views/listePromotion.jsp";
                } else {
                    if ("addPromotion".equals(switchAction)) {
                        req.setAttribute("addPromotion", "");
                        viewName = "/WEB-INF/views/addPromotion.jsp";
                    } else {
                        if ("displayAllMatiere".equals(switchAction)) {
                            req.setAttribute("listeDeMatiere", "");
                            viewName = "/WEB-INF/views/listeMatiere.jsp";
                        } else {
                            if ("displayAllCours".equals(switchAction)) {
                                String lsNomFichier = getServletContext().getRealPath("/resources/cours.csv");
                                req.setAttribute("listeDeCour", coursService.loadAllCours(lsNomFichier));
                                viewName = "/WEB-INF/views/listeCour.jsp";
                            } else {
                                if ("displayAllAbsence".equals(switchAction)) {
                                    req.setAttribute("listeAbsence", "");
                                    viewName = "/WEB-INF/views/listeAbsence.jsp";
                                } else {
                                    if ("addUser".equals(switchAction)) {
                                        req.setAttribute("addUser", "");
                                        viewName = "/WEB-INF/views/addUser.jsp";
                                    } else {
                                        if ("displayAllUser".equals(switchAction)) {
                                            String lsNomFichier = getServletContext().getRealPath("/resources/login_mdp.csv");
                                            req.setAttribute("listeAllUser", userService.loadAllLogin(lsNomFichier));
                                            viewName = "/WEB-INF/views/listeUser.jsp";
                                        } else {
                                            if ("homeAdmin".equals(switchAction)) {
                                                req.setAttribute("listeDePersonne", adminServices.returnPersonneDAO());
                                                req.setAttribute("listeEtudiant", adminServices.returnPersonneEtudiantDAO());
                                                req.setAttribute("listeMatiere", adminServices.getAllExistingMatiereContentCours());
                                                req.setAttribute("listeAbsence", adminServices.getAllAbsenceEtudiantDAO());
                                                viewName = "/WEB-INF/views/admin.jsp";
                                            } else {
                                                req.setAttribute("listeDePersonne", personneService.returnPersonneDAO());
                                                viewName = "/WEB-INF/views/admin.jsp";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
        displayJspPage(viewName, req, resp);

    }

    public void displayJspPage(String pageLocation, ServletRequest req, ServletResponse res) throws ServletException, IOException {
        RequestDispatcher disp = this.getServletContext().getRequestDispatcher(pageLocation);
        disp.forward(req, res);
    }
}
package com.sqli.ecdf.services;

import com.sqli.ecdf.model.Absence;
import com.sqli.ecdf.model.Matiere;
import com.sqli.ecdf.model.Personne;
import com.sqli.ecdf.model.Promotion;
import java.util.List;
import java.util.Map;

public interface AdminsServices {

    public Map<String, Matiere> getOneMatiereListCours(String nom);

    public List<Matiere> getAllExistingMatiereContentCours();
    
    public Map<String, Personne> returnPersonneDAO();
    
    public Map<String, Personne> returnAlonePersonneDAO(String nom, String prenom);

    public Map<String, Personne> returnPersonneEtudiantDAO();

    public Map<String, Personne> returnAlonePersonneEtudiantDAO(String nom, String prenom);

    public Map<String, Absence> getAllAbsenceEtudiantDAO();

    public Map<String, Absence> getAllAbsenceUnEtudiantDAO(String nom, String prenom);
    
    public Map<String, Promotion>getAllPromotionList();
    
    public Map<String, Promotion> getOnePromotionList(String libelle);
}

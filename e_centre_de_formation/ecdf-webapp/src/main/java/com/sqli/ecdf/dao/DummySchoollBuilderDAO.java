/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.Admin;
import com.sqli.ecdf.model.Adresse;
import com.sqli.ecdf.model.Enseignant;
import com.sqli.ecdf.model.Etudiant;
import com.sqli.ecdf.model.LoginInfos;
import com.sqli.ecdf.model.Personne;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;

/**
 *
 * @author stagiaire
 */
public class DummySchoollBuilderDAO extends ManagedSchool {
    

    public Map<String, Personne> returnPersonneDAO() {

        Map<String, Personne> personnes = new HashMap<String, Personne>();
        
        Personne admin1 = new Admin("nom1", "prenom1", "email1", "photo1", new Adresse("rue1", 75000, "Paris"), new LoginInfos("mdp1", "login1"));
        Personne admin2 = new Admin("nom2", "prenom2", "email2", "photo2", new Adresse("rue2", 05600, "Saint Firmin"), new LoginInfos("mdp2", "login2"));
        Personne enseignant1 = new Enseignant("nom3", "prenom1", "email1", "photo1", new Adresse("rue3", 92000, "Boulogne"), new LoginInfos("mdp3", "login3"));
        Personne enseignant2 = new Enseignant("nom2", "prenom2", "email2", "photo2", new Adresse("rue2", 05600, "Saint Firmin"), new LoginInfos("mdp2", "login2"));
        Personne etudiant1 = new Etudiant("nom1", "prenom1", "email1", "photo1", new Adresse("rue2", 05600, "Saint Firmin"), new LoginInfos("mdp2", "login2"), new Date(2013 / 12 / 05));
        Personne etudiant2 = new Etudiant("nom2", "prenom2", "email2", "photo2", new Adresse("rue2", 05600, "Saint Firmin"), new LoginInfos("mdp2", "login2"),new Date(2013 / 12 / 05));
        
        personnes.put(admin1.returnKey(), admin1);
        personnes.put(admin2.returnKey(), admin2);
        personnes.put(enseignant1.returnKey(), enseignant1);
        personnes.put(enseignant2.returnKey(), enseignant2);
        personnes.put(etudiant1.returnKey(), etudiant1);
        personnes.put(etudiant2.returnKey(), etudiant2);
        
        return personnes;
    }
    
    public Map<String, Personne> returnAlonePersonneDAO(String nom, String prenom) {

        Map<String, Personne> personnes = returnPersonneDAO();
        Map<String, Personne> alonePersonne = new HashMap<String, Personne>();
        
        for (Personne personne: personnes.values()){
            if (personne.getNom().equals(nom) && personne.getPrenom().equals(prenom)){
                alonePersonne.put(personne.returnKey(), personne);
                break;
            }
        }
        
        if (personnes.size() == 0){
            Personne pers = new Admin("nom1", "prenom1", "email1", "photo1", new Adresse("rue1", 75000, "Paris"), new LoginInfos("mdp1", "login1"));
            alonePersonne.put(pers.returnKey(), pers);
        }
        
        return alonePersonne;
    
    }
    
}

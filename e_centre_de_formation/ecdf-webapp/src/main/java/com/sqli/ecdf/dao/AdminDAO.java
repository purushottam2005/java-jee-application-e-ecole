package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.Absence;
import com.sqli.ecdf.model.Admin;
import com.sqli.ecdf.model.Adresse;
import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.Enseignant;
import com.sqli.ecdf.model.Etudiant;
import com.sqli.ecdf.model.exception.KeyAlreadyThereException;
import com.sqli.ecdf.model.LoginInfos;
import com.sqli.ecdf.model.Personne;
import com.sqli.ecdf.services.AdminServices;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AdminDAO extends AdminServices{
      public AdminDAO() throws KeyAlreadyThereException{
        super();
          init();
    }
    
       public void init() throws KeyAlreadyThereException{
                
      /*  Personne admin1 = new Admin("nom1", "prenom1", "email1", "photo1", new Adresse("rue1", 75000, "Paris"), new LoginInfos("mdp1", "login1"));
        Personne admin2 = new Admin("nom2", "prenom2", "email2", "photo2", new Adresse("rue2", 05600, "Saint Firmin"), new LoginInfos("mdp2", "login2"));
        Personne enseignant1 = new Enseignant("nom1", "prenom1", "email1", "photo1", new Adresse("rue3", 92000, "Boulogne"), new LoginInfos("mdp3", "login3"));
        Personne enseignant2 = new Enseignant("nom2", "prenom2", "email2", "photo2", new Adresse("rue2", 05600, "Saint Firmin"), new LoginInfos("mdp2", "login2"));
        Personne etudiant1 = new Etudiant("nom1", "prenom1", "email1", "photo1", new Adresse("rue2", 05600, "Saint Firmin"), new LoginInfos("mdp2", "login2"), new Date(2013 / 12 / 05));
        Personne etudiant2 = new Etudiant("nom2", "prenom2", "email2", "photo2", new Adresse("rue2", 05600, "Saint Firmin"), new LoginInfos("mdp2", "login2"),new Date(2013 / 12 / 05));
        Personne etudiant3 = new Etudiant("nom3", "prenom3", "email2", "photo2", new Adresse("rue2", 05600, "Saint Firmin"), new LoginInfos("mdp3", "login23"),new Date(2013 / 12 / 05));
        
        
        addPersonne(admin1);
        addPersonne(admin2);
        addPersonne(enseignant1);
        addPersonne(enseignant2);
        addPersonne(etudiant1);
        addPersonne(etudiant2);
        
        Cours cours1 = new Cours(new Date(),120,"JAVA JEE");
        Absence absence1 = new Absence("maladie", cours1,(Etudiant)etudiant1);
        this.addCours(cours1);
        this.addAbsence("maladie rare", absence1);
        
        Cours cours2 = new Cours(new Date(),160,"HTML CSS");
        Absence absence2 = new Absence("maladie", cours1,(Etudiant)etudiant2);
        this.addCours(cours2);
        this.addAbsence("infectieuse", absence2);
        
        Cours cours3 = new Cours(new Date(),140,"COBOL");
        Absence absence3 = new Absence("sida", cours3,(Etudiant)etudiant3);
        this.addCours(cours3);
        this.addAbsence("mortel", absence3);
        */
        
        
    }

        public Map<String, Personne> returnAlonePersonneDAO(String nom, String prenom) {
        
        Map<String, Personne> personnes = this.getAllPersonne();
        Map<String, Personne> alonePersonne = new HashMap<String, Personne>();
        
        for (Personne personne: personnes.values()){
            if (personne.getNom().equals(nom) && personne.getPrenom().equals(prenom)){
                alonePersonne.put(personne.returnKey(), personne);
                break;
            }
        }
        
        if (personnes.size() == 0){
            Personne pers = new Admin("", "", "", "", new Adresse("", 0, ""), new LoginInfos("", ""));
            alonePersonne.put(pers.returnKey(), pers);
        }
        
        return alonePersonne;
    
    }
      public Map<String, Personne> returnAloneUserPersonneDAO(String loginUser, String loginPassword) {

        Map<String, Personne> personnes = this.getAllPersonne();
        Map<String, Personne> personneOne = new HashMap<String, Personne>();
       
        for (Personne personne: personnes.values()){
            if (personne.getLoginInfos().getLogin().equals(loginUser) && personne.getLoginInfos().getMotDePasse().equals(loginPassword)){
                personneOne.put(personne.returnKey(), personne);
             
                break;
            }
        }
        if (personnes.size() == 0){
            Personne pers = new Admin("", "", "", "", new Adresse("", 0, ""), new LoginInfos("", ""));
            personneOne.put(pers.returnKey(), pers);
        }
    
        return personneOne;
    }
}

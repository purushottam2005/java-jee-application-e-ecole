package com.sqli.ecdf.services;

import com.sqli.ecdf.dao.ManagedSchool;
import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.Etudiant;


public class EnseignantService implements InterfaceEnseignant {
    
    private ManagedSchool ecole ;

    public EnseignantService(ManagedSchool ecole) {
        this.ecole = ecole;
    }
    

    public void addCoursContents(Cours cours, String description) {
    }

    public void removeCoursContents(Cours cours, String description) {
    }

    public void addAbsence(String motif, Cours cours, Etudiant etudiant) {
    }

    public void removeAbsence(String motif, Cours cours, Etudiant etudiant) {
    }
    
}

package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.Cours;
import java.util.List;
import java.util.Set;

public interface CoursDAO {

    public Set<Cours> loadAllCours(String lsNomFichier);
    public Set<Cours> loadAgainAllCours();
    public List<Cours>getAllExistingCours();
}

package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.Adresse;
import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.Etudiant;
import com.sqli.ecdf.model.LoginInfos;
import com.sqli.ecdf.model.Promotion;
import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DummyPromotionDAO implements PromotionDAO {
    //-- Methode pour la conversion des dates
    public static Date parseDate(String date) throws ParseException {
        Date result = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat();
            result = formatter.parse(date);
            return result;
        } catch (RuntimeException e) {
            return null;
        }
    }
    
    //-- Utilisation du fichier CSV
    @Override
    public Set<Promotion> loadAllPromotion(String lsNomFichier) {
        StringBuilder lsbContenu = new StringBuilder();
        Set<Promotion> tLignesPromo = new HashSet<>();
        ArrayList tAllLignes = new ArrayList();

        try {
//            lsNomFichier = "/resources/promotions.csv";
            // --- Lecture par ligne
            FileReader lfrFichier;
            BufferedReader lbrBuffer;
            String lsLigne;
            String[] tChamps;


            lfrFichier = new FileReader(lsNomFichier);
            lbrBuffer = new BufferedReader(lfrFichier);
            while ((lsLigne = lbrBuffer.readLine()) != null) {
                tAllLignes.add(lsLigne);
            }
            for (int i = 1; i < tAllLignes.size(); i++) {
                tChamps = tAllLignes.get(i).toString().split(";");

                Promotion promotions = new Promotion(tChamps[0]);
                tLignesPromo.addAll(Arrays.asList(promotions));
            }

            lbrBuffer.close();
            lfrFichier.close();
        } catch (Exception err) {
            lsbContenu.append(err.getMessage());
        }
        return tLignesPromo;
    }
    
    @Override
    public Map<String, Promotion>getAllPromotionList(){
        Map<String, Promotion> promotions = new HashMap<>();
         DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date1 = df.parse("20/12/2012");
            Date date2 = df.parse("01/08/2012");
            Date date3 = df.parse("20/09/2012");
            Date date4 = df.parse("15/12/2012");
            Date date5 = df.parse("05/02/2013");
            
        Etudiant etudiant1 = new Etudiant("KANDATE", "Gervil", "email3", "/images/personnes/homme-img.jpg", new Adresse("105 Av de Java", 74500, "Oracle"), new LoginInfos("mdpg", "gervil"), new Date(2013 / 12 / 05));
        Etudiant etudiant2 = new Etudiant("NGOKIO", "Christian", "email4", "/images/personnes/homme-img.jpg", new Adresse("3 Rue de Lille", 93400, "Saint Ouen"), new LoginInfos("mdpcri", "christian"), new Date(2013 / 12 / 05));
        Etudiant etudiant3 = new Etudiant("JUNKER", "Nicolas", "email5", "/images/personnes/homme-img.jpg", new Adresse("256 rue de Paris", 92600, "Boulogne"), new LoginInfos("mdpnic", "nicolas"), new Date(2013 / 12 / 05));

            
            Cours cours1 = new Cours(date1, 420, "JAVA les bases");
            Cours cours2 = new Cours(date2, 380, "HTML les bases");
            Cours cours3 = new Cours(date3, 380, "MAVEN");
            Cours cours4 = new Cours(date4, 210, "ANDROID");
            Cours cours5 = new Cours(date5, 80, "CSS3");

       Promotion promotion1 = new Promotion("Developpeur JAVA");
      Promotion promotion2 = new Promotion("Developpeur PHP");
            
            promotion1.addCours(cours1);
            promotion1.addCours(cours2);
            promotion1.addCours(cours3);
            promotion1.addCours(cours4);
            
            promotion2.addCours(cours2);
            promotion2.addCours(cours5);
            promotion2.addCours(cours4);
            try {
                promotion1.addEtudiant(etudiant1);
                promotion2.addEtudiant(etudiant2);
                promotion1.addEtudiant(etudiant3);
            } catch (Exception ex) {
                Logger.getLogger(DummyPromotionDAO.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (ParseException ex) {
            Logger.getLogger(DummyCoursDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return promotions;
    }
    
    @Override
    public Map<String, Promotion> getOnePromotionList(String libelle){
        Map<String, Promotion> allPromotion = getAllPromotionList();
        Map<String, Promotion> onePromotion = new HashMap<>();

        for (Promotion promotion : allPromotion.values()) {
            if (promotion.getLibelle().equals(libelle)) {
                onePromotion.put(promotion.getLibelle(), promotion);
            }
        }
        return onePromotion;
    }
}


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.ecdf.webapp.controleur;

import com.sqli.ecdf.model.Absence;
import com.sqli.ecdf.model.Matiere;
import com.sqli.ecdf.model.Personne;
import com.sqli.ecdf.services.DummyEtudiantServices;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebServlet(name = "EtudiantServlet", urlPatterns = {"/ListeEtudiant"})
public class EtudiantServlet extends HttpServlet {

    @Setter
    private static DummyEtudiantServices serviceEtudiant;

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        String viewName;
        HttpSession session = null;
        String switchAction = req.getParameter("nav");
        String nom = req.getParameter("nom");
        String prenom = req.getParameter("prenom");
        String user = req.getParameter("user");
        String cours = req.getParameter("cours");


        if ("displayAllCoursEtudiant".equals(switchAction)) {
//            List<Matiere> allMatiereCours = serviceEtudiant.getAllExistingMatiereContentCours();
            List<Matiere> allMatiereCours = serviceEtudiant.getAllExistingMatiereContentCours();
            Map<String, Matiere> allMatiere = serviceEtudiant.getAllMatiereListCours();
            System.out.println(" matiere" + serviceEtudiant.getAllMatiereListCours());

            req.setAttribute("infoEtudiant", serviceEtudiant.returnOnePersonneEtudiantDAO(user));
            req.setAttribute("listeMatieresCours", allMatiereCours);
            req.setAttribute("listeMatieres", allMatiere);
            viewName = "/WEB-INF/views/listeCourEtudiant.jsp";

        } else {
            if ("displayAllAbsenceEtudiant".equals(switchAction)) {
                Map<String, Absence> oneUserAbsence = serviceEtudiant.getAllAbsenceUnEtudiantDAO(nom, prenom);
                req.setAttribute("listeAbsenceEtudiant", oneUserAbsence);
                req.setAttribute("infoEtudiant", serviceEtudiant.returnOnePersonneEtudiantDAO(user));
                System.out.println("absence etudiant {}" + serviceEtudiant.returnOnePersonneEtudiantDAO(user));
                viewName = "/WEB-INF/views/listeAbsenceEtudiant.jsp";
            } else {
                if ("homeStudent".equals(switchAction)) {
                    Set<Absence> allAbsences = serviceEtudiant.getAllAbsence();
                    req.setAttribute("listeAbsenceEtudiant", allAbsences);
                    viewName = "/WEB-INF/views/etudiant.jsp";
                } else {
                    if ("displayAllContentCoursPdf".equals(switchAction)) {
                        req.setAttribute(cours, cours);
                              viewName = "/WEB-INF/views/listeCoursPdf.jsp";
                    } else {
                        req.removeAttribute(user);
                        viewName = "/index.jsp";
                    }
                }
            }
        }
        displayJspPage(viewName, req, res);
    }

    public void displayJspPage(String pageLocation, ServletRequest req, ServletResponse res) throws ServletException, IOException {
        RequestDispatcher disp = this.getServletContext().getRequestDispatcher(pageLocation);
        disp.forward(req, res);

    }
}

package com.sqli.ecdf.services;

import com.sqli.ecdf.model.Absence;
import com.sqli.ecdf.model.Admin;
import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.Enseignant;
import com.sqli.ecdf.model.Etudiant;
import com.sqli.ecdf.model.exception.KeyAlreadyThereException;
import com.sqli.ecdf.model.exception.KeyNotThereException;
import com.sqli.ecdf.model.LoginInfos;
import com.sqli.ecdf.model.Matiere;
import com.sqli.ecdf.model.Personne;
import com.sqli.ecdf.model.Promotion;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AdminServices implements InterfaceAdmin, EnseignantServices{

    private Map<String, LoginInfos> listPersonneEtLogin;
    private Map<String, Personne> listPersonne;
    private Map<String, Absence> listAbsence;
    private Map<String, Matiere> listMatiere;
    private Map<String, Promotion> listPromotion;
    private Map<String, Cours> listCours;

    public AdminServices() {
        this.listPersonneEtLogin = new HashMap<String, LoginInfos>();
        this.listPersonne = new HashMap<String, Personne>();
        this.listAbsence = new HashMap<String, Absence>();
        this.listMatiere = new HashMap<String, Matiere>();
        this.listPromotion = new HashMap<String, Promotion>();
        this.listCours = new HashMap<String, Cours>();
    }

    public Map<String,Personne> getAllPersonne() {
        return Collections.unmodifiableMap(listPersonne);
    }
    
    public Set<Etudiant> getAllEtudiant() {
        Set<Etudiant> result;
        result = new HashSet<Etudiant>();
        for (Personne personne : listPersonne.values()) {
            if (personne instanceof Etudiant) {
                result.add((Etudiant) personne);
            }
        }
        return Collections.unmodifiableSet(result);
    }

    public Set<Admin> getAllAdmins() {
        Set<Admin> result;
        result = new HashSet<Admin>();

        for (Personne personne : listPersonne.values()) {
            if (personne instanceof Admin) {
                result.add((Admin) personne);
            }
        }
        return Collections.unmodifiableSet(result);
    }

    public Set<Enseignant> getAllEnseignants() {
        Set<Enseignant> result;
        result = new HashSet<Enseignant>();

        for (Personne personne : listPersonne.values()) {
            if (personne instanceof Enseignant) {
                result.add((Enseignant) personne);
            }
        }
        return Collections.unmodifiableSet(result);
    }

    public Set<Matiere> getAllMatieres() {
        Set<Matiere> result;
        result = new HashSet<Matiere>();
        for (Matiere matiere : listMatiere.values()) {
            result.add(matiere);
        }
        return Collections.unmodifiableSet(result);

    }

        public Set<Absence> getAllAbsences() {
        Set<Absence> result;
        result = new HashSet<Absence>();
        for (Absence absence : listAbsence.values()) {
            result.add(absence);
        }
        return Collections.unmodifiableSet(result);
    }
    
        public Set<Cours> getAllCours(){
        Set<Cours> result;
        result = new HashSet<Cours>();
        for (Cours cours: this.listCours.values()){
            result.add(cours);
        }
        
        return Collections.unmodifiableSet(result);
    }

    @Override
    public void addPersonne(Personne personne) throws KeyAlreadyThereException {
        if (listPersonne.containsKey(personne.returnKey())) {
            throw new KeyAlreadyThereException("add personne failed");
        } else {
            this.listPersonneEtLogin.put(personne.returnKey(), personne.getLoginInfos());
		this.listPersonne.put(personne.returnKey(), personne);
        }
    }

    @Override
        public void removePersonne(String keyPersonne) throws KeyNotThereException {
        if (!listPersonneEtLogin.containsKey(keyPersonne)) {
            throw new KeyNotThereException("remove personne failed !");

        }
        listPersonneEtLogin.remove(keyPersonne);
        listPersonne.remove(keyPersonne);
    }

    @Override
    public void addMatiere(String nom, Matiere matiere) throws KeyAlreadyThereException {
        if (!listMatiere.containsKey(nom)) {
            listMatiere.put(nom, matiere);
        } else {
            throw new KeyAlreadyThereException("add matiere failed !");
        }
    }

    @Override
    public void removeMatiere(String nom) throws KeyNotThereException {
        if (!listMatiere.containsKey(nom)) {
            throw new KeyNotThereException("matiere doesn't exist !");
        } else {
            listMatiere.remove(nom);
        }
    }

    @Override
    public Matiere getMatiere(String nom) throws KeyNotThereException {
        if (!listMatiere.containsKey(nom)) {
            throw new KeyNotThereException("matiere doesn't exist !");
        } else {
            return listMatiere.get(nom);
        }

    }

    @Override
    public void addPromotion(String nomPromotion, Promotion promotion) throws KeyAlreadyThereException {
        if (!listPromotion.containsKey(nomPromotion)) {
            throw new KeyAlreadyThereException("promotion already there exist");
        } else {
            listPromotion.put(nomPromotion, promotion);
        }
    }

    @Override
        public void removePromotion(String nomPromotion) throws KeyNotThereException {
        if (!listPromotion.containsKey(nomPromotion)) {
            throw new KeyNotThereException("remove promotion failed!");
        } else {
            listPromotion.remove(nomPromotion);
        }
    }

    @Override
    public void addCours(Cours cours) throws KeyAlreadyThereException {
        if (!listCours.containsKey(cours)) {
            listCours.put(cours.getDescription(), cours);
        } else {
            throw new KeyAlreadyThereException("add cours failed !");
        }
    }

    @Override
    public void removeCours(String description) throws KeyNotThereException {
        if (!listCours.containsKey(description)) {
            throw new KeyNotThereException("cours doesn't exists !");
        } else {
            listCours.remove(description);
        }

    }

    public static String formatKeyAbsence(String motif, Cours cours, Personne personne) {
        return motif + "-" + cours.getDescription() + "-" + personne.getNom() + "-" + personne.getPrenom();
    }

    @Override
    public void addAbsence(String absenceName, Absence absence) throws KeyAlreadyThereException {
        if (!listAbsence.containsKey(absenceName)) {
            listAbsence.put(absenceName, absence);
        } else {
            throw new KeyAlreadyThereException("add absence failed !");
        }
    }

    @Override
    public void removeAbsence(String motif) throws KeyNotThereException {
        if (!listAbsence.containsKey(motif)) {
            throw new KeyNotThereException("absence doesn't exist !");
        } else {
            listAbsence.remove(motif);
        }
    }

    @Override
    public void addCoursContents(Cours cours, String description) {
        cours.setDescription(description);
    }

    @Override
    public void removeCoursContents(Cours cours, String description) {
        cours.setDescription("no description");
    }

  
    @Override
    public void addAbsence(String motif, Cours cours, Etudiant etudiant) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeAbsence(String motif, Cours cours, Etudiant etudiant) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Matiere> getAllExistingMatiereContentCours() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, Personne> returnPersonneEtudiantDAO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, Personne> returnAlonePersonneEtudiantDAO(String nom, String prenom) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, Absence> getAllAbsenceEtudiantDAO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, Absence> getAllAbsenceUnEtudiantDAO(String nom, String prenom) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

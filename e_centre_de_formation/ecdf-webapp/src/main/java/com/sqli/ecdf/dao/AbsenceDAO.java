package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.Absence;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface AbsenceDAO {

    public Set<Absence> getAllAbsences();

    public List<Absence> getAllExistingAbsence();

    public Map<String, Absence> getAllAbsenceEtudiantDAO();

    public Map<String, Absence> getAllAbsenceUnEtudiantDAO(String nom, String prenom);

    public Map<String, Absence> getAllAbsenceOneUser(String userName);
}

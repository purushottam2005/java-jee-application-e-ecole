/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.ecdf.webapp.controleur;

import com.sqli.ecdf.dao.DummySchoollBuilderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author stagiaire
 */
@Slf4j
@WebServlet(name = "UpdatePersonneServlet", urlPatterns = {"/UpdatePersonneServlet"})
public class UpdatePersonneServlet extends HttpServlet {

    @Setter
    private static DummySchoollBuilderDAO personneService;

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nom = req.getParameter("nom");
        String prenom = req.getParameter("prenom");
        req.setAttribute("alonePersonne", personneService.returnAlonePersonneDAO(nom, prenom));
        req.setAttribute("listeDePersonne", personneService.returnPersonneDAO());
        //log.info("personneService.returnAlonePersonneDAO().size() {}", personneService.returnAlonePersonneDAO(nom, prenom).size());
        String viewName = "/WEB-INF/views/admin.jsp";
        displayJspPage(viewName, req, resp);
    }

    public void displayJspPage(String pageLocation, ServletRequest req, ServletResponse res) throws ServletException, IOException {
        RequestDispatcher disp = this.getServletContext().getRequestDispatcher(pageLocation);
        disp.forward(req, res);

    }
}

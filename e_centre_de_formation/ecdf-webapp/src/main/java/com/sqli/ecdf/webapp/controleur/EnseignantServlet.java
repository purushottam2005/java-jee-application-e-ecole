/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.ecdf.webapp.controleur;

import com.sqli.ecdf.dao.DummyCoursDAO;
import com.sqli.ecdf.services.DummyEnseignantServices;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lombok.Setter;

@WebServlet(name = "EnseignantServlet", urlPatterns = {"/ListeEnseignant"})
public class EnseignantServlet extends HttpServlet {

    @Setter
    private static DummyCoursDAO coursService;
    @Setter
    private static DummyEnseignantServices enseignantServices;

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String nom = req.getParameter("nom");
        String prenom = req.getParameter("prenom");
        String user = req.getParameter("user");
        String cours = req.getParameter("cours");

        String viewName;
        HttpSession session = null;
        String switchAction = req.getParameter("nav");
        if ("displayAllCours".equals(switchAction)) {
            req.setAttribute("listeDeCours", coursService.loadAgainAllCours());
            req.setAttribute("listeMatiereContenuCours", enseignantServices.getAllExistingMatiereContentCours());
            viewName = "/WEB-INF/views/listeCourEnseignant.jsp";
        } else {
            if ("displayAllAbsence".equals(switchAction)) {
                req.setAttribute("listeEtudiantEtAbsence", enseignantServices.returnPersonneEtudiantDAO());
                req.setAttribute("listeEtudiantEtAbsenceUn", enseignantServices.returnAlonePersonneEtudiantDAO(nom, prenom));
                req.setAttribute("listeAbsenceAll", enseignantServices.getAllAbsenceUnEtudiantDAO(nom, prenom));

                viewName = "/WEB-INF/views/listeAbsencePourEnseignant.jsp";
            } else {
                if ("homeTeacher".equals(switchAction)) {
                    req.setAttribute("listeMatiereContenuCours", enseignantServices.getAllExistingMatiereContentCours());
                    viewName = "/WEB-INF/views/enseignant.jsp";
                } else {
                    if ("displayAllContentCoursPdf".equals(switchAction)) {
                        req.setAttribute(cours, cours);
                        viewName = "/WEB-INF/views/listeCoursPdf.jsp";
                    } else {
                        req.removeAttribute(user);
                        viewName = "/index.jsp";
                    }
                }
            }
        }
        displayJspPage(viewName, req, resp);
    }

    public void displayJspPage(String pageLocation, ServletRequest req, ServletResponse res) throws ServletException, IOException {
        RequestDispatcher disp = this.getServletContext().getRequestDispatcher(pageLocation);
        disp.forward(req, res);

    }
}

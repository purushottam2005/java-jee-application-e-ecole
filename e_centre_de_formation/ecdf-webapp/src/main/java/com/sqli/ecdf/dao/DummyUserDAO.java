package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.LoginInfos;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DummyUserDAO implements UserDAO {

    @Override
    public Set<LoginInfos> loadAllLogin(String lsNomFichier) {
        StringBuilder lsbContenu = new StringBuilder();
        Set<LoginInfos> tLignesLogin = new HashSet<>();
        ArrayList tAllLignes = new ArrayList();

        try {
            FileReader lfrFichier;
            BufferedReader lbrBuffer;
            String lsLigne;
            String[] tChamps;


            lfrFichier = new FileReader(lsNomFichier);
            lbrBuffer = new BufferedReader(lfrFichier);
            while ((lsLigne = lbrBuffer.readLine()) != null) {
                tAllLignes.add(lsLigne);
            }
            for (int i = 1; i < tAllLignes.size(); i++) {
                tChamps = tAllLignes.get(i).toString().split(";");
                LoginInfos login = new LoginInfos(tChamps[1], tChamps[0]);
                tLignesLogin.addAll(Arrays.asList(login));

            }

            lbrBuffer.close();
            lfrFichier.close();
        } catch (Exception err) {
            lsbContenu.append(err.getMessage());
        }
        return tLignesLogin;
    }

    public void writeFile(String fileNames, String loginValues) {

        try {
            FileWriter lfwFichier = new FileWriter(fileNames, true);
            BufferedWriter lbwBuffer = new BufferedWriter(lfwFichier);


            lbwBuffer.write(loginValues + "\r\n");
            // lbwBuffer.newLine();
            // lbwBuffer.write("login;mdp\r\n"); // --- Alternative

            lbwBuffer.close();
            lfwFichier.close();

            System.out.println("Fin de l'écriture d'un fichier texte");
        } catch (IOException e) {
            System.err.println(e);
        }

    }

    public boolean compareLogin(String enterKeys, String fileNames) {
        File f = new File(fileNames);
        String fileContains;
        try {

            if (f.exists()) {
                log.warn("File existed");
                fileContains = fileToString(fileNames);
//               fileContains = file_get_contents(fileNames);
                if (fileContains.equalsIgnoreCase(enterKeys)) {
//                  $lsMessage = "<br/>Saisies correctes, vous êtes authentifi(e)";
//                  setCookie("qualite", "abonne");
                } else {
//                  $lsMessage  = "<br/>Saisies incorrectes, vous n'êtes pas authentifié(e)";
                }
            } else {
                log.warn("File not found!");
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return false;
    }

    // Converts a file to a string Equivalent de file_get_contents PHP
    private String fileToString(String filename) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        StringBuilder builder = new StringBuilder();
        String line;

        // For every line in the file, append it to the string builder
        while ((line = reader.readLine()) != null) {
            builder.append(line);
        }

        return builder.toString();
    }

    @Override
    public List<LoginInfos> userList() {

        List<LoginInfos> userList = new ArrayList();

        LoginInfos admin1 = new LoginInfos("mdpa", "andry");
        LoginInfos admin2 = new LoginInfos("mdpsoph", "sophie");
        
        LoginInfos etudiant1 = new LoginInfos("mdpg", "gervil");
        LoginInfos etudiant2 = new LoginInfos("mdpcri", "christian");
        LoginInfos etudiant3 = new LoginInfos("mdpnic", "nicolas");
        
        LoginInfos enseignant1 = new LoginInfos("mdpseb", "sebastien");
        
        userList.add(admin1);
        userList.add(admin2);
        userList.add(etudiant1);
        userList.add(etudiant2);
        userList.add(etudiant3);
        userList.add(enseignant1);

        return userList;

    }

    
    
    @Override
    public boolean isIdentifier(String loginForm, String passwordForm) {
        // on récupère la liste dans la base de donnée (ici en dur)
        List<LoginInfos> listUser = userList();
        //on parcour la liste et on teste les entrées du formulaire.
        for (int i = 0; i < listUser.size(); i++) {
            if (loginForm.equalsIgnoreCase(listUser.get(i).getMotDePasse()) & passwordForm.equalsIgnoreCase(listUser.get(i).getLogin())) {
                return true;
            }
        }
        return false;
    }
}

package com.sqli.ecdf.services;

import com.sqli.ecdf.dao.ManagedSchool;
import com.sqli.ecdf.model.Cours;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EtudiantService implements InterfaceEtudiant{
    private ManagedSchool ecole;

    public EtudiantService(ManagedSchool ecole) {
        this.ecole = ecole;
    }
    

    

    public void displayAbsence() {
        ecole.getAllAbsences();
    }

    public String displayCours(Cours cours) {
        return cours.getDescription();
        
    }
    
}

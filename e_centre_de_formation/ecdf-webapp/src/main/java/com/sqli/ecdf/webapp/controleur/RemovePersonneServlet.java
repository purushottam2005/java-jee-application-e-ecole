/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.ecdf.webapp.controleur;

import com.sqli.ecdf.dao.DummyPersonneDAO;
import com.sqli.ecdf.model.Personne;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author stagiaire
 */
@Slf4j
@WebServlet(name = "RemovePersonneServlet", urlPatterns = {"/RemovePersonneServlet"})
public class RemovePersonneServlet extends HttpServlet {

    @Setter
    public static DummyPersonneDAO serviceDummyPersonneDAO;
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       String action = req.getParameter("action");
       String viewName = "";
       if (req.getParameter("action").equals("delete")){
           String nom = req.getParameter("nomForm");
           String prenom = req.getParameter("prenomForm");
           String role = req.getParameter("idRole");
           Personne pers = serviceDummyPersonneDAO.getPersonne(nom, prenom, role);
           serviceDummyPersonneDAO.removePersonne(pers);
           viewName = "/WEB-INF/views/listePersonne.jsp";
           log.info("action {}", action);
           log.info("nom {}", nom);
           log.info("prenom {}", prenom);
           log.info("role {}", role);
       }
       
       displayJspPage(viewName, req, resp);
    }
    
    public void displayJspPage(String pageLocation, ServletRequest req, ServletResponse res) throws ServletException, IOException {
        RequestDispatcher disp = this.getServletContext().getRequestDispatcher(pageLocation);
        disp.forward(req, res);

    }

  
}

package com.sqli.ecdf.services;

import com.sqli.ecdf.model.Cours;

public interface InterfaceEtudiant {
    
    String displayCours(Cours cours);
    void displayAbsence();
    
}

package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.Personne;
import java.util.Map;

public interface PersonneDAO {

    public Map<String, Personne> returnPersonneDAO();

    public Map<String, Personne> returnAlonePersonneDAO(String nom, String prenom);

    public String returnAloneUserPersonneDAO(String loginUser);

    public boolean isIdentification(String loginUser, String loginPassword);

    public Map<String, Personne> returnPersonneEtudiantDAO();

    public Map<String, Personne> returnAlonePersonneEtudiantDAO(String nom, String prenom);

    public Map<String, Personne> returnOnePersonneEtudiantDAO(String loginUser);
}

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix= "fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:url var= "accueil" value="/HomeServlet" />
<c:url var= "user" value="/Users" />
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.png">

        <title>site centre de formation</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/justified-nav.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

    </head>

    <body>

        <div class="container">

            <div class="masthead">
                <!-- Menu navigation -->
                <jsp:include page="/WEB-INF/views/common/menuAdmin.jsp" flush="true"/>
                <!-- Fin menu navigation -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel-heading"></div>
                        <div class="panel panel-primary">
                            <div class="panel-heading">UTILISATEUR : AJOUT</div>
                            <div class="panel-body">

                                <form class="form-horizontal" role="form" method="POST" action="${user}">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label for="inputText3" class="col-sm-3 control-label">Login</label>
                                            <div class="col-sm-9">
                                                <input type="text" name="login" class="form-control" placeholder="Text input">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputText3" class="col-sm-3 control-label">Mot de passe</label>
                                            <div class="col-sm-9">
                                                <input type="password" name="password" class="form-control" placeholder="Text input">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-10">
                                                <button type="submit" class="btn btn-success">Ajouter</button>
                                            </div>
                                        </div>
                                        <c:if test="${!empty param}">
                                            <c:if test="${empty param['login']}">
                                                <p class='alert-danger'>
                                                    <c:out  value="nom d'utilisateur obligatoire!!" />
                                                </p>
                                            </c:if>
                                            <c:if test="${empty param['password']}">
                                                <p class='alert-danger'>
                                                    <c:out  value="mot de passe obligatoire!!" />
                                                </p>
                                            </c:if>
                                        </c:if>
                                    </form>
                            </div>
                        </div>
                    </div>
                    <!-- Site footer -->
                    <div class="footer">
                        <p></p>
                    </div>
                </div>

            </div> <!-- /container -->


            <!-- Bootstrap core JavaScript
            ================================================== -->
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/bootstrap.js"></script>
    </body> 
</html>

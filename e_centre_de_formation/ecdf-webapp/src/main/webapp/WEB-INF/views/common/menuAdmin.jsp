<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix= "fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:url var= "logout" value="/Logout" />
<c:url value="/ListeAdmin" var="displayUser" />
<c:url var= "listeAbsences" value="/ListAbsencesServlet" />

<h3 class="text-muted">ESPACE ADMINISTRATEUR
    <span class="label label-success">
        <c:if test="${!empty sessionScope.user}">
            Bienvenue ${sessionScope.user}</span>
        <span>
            <button type="button" class="btn btn-default btn-lg">
                <a href="${logout}"><span class="glyphicon glyphicon-user"> </span> Deconnexion</a>
            </button>
        </span>

    </c:if>
</h3>
<nav class="nav nav-justified" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
                    <c:url value="/ListeAdmin" var="accueil">
                        <c:param name="nav" value="homeAdmin" />
                    </c:url>
        <a class="navbar-brand" href="${accueil}">Accueil</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gestion des personnes <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <c:url value="/ListeAdmin" var="addAdmin">
                        <c:param name="nav" value="addPerson" />
                    </c:url>
                    <li><a href="${addAdmin}">Ajouter personne</a></li>
                    <li class="divider"></li>
                        <c:url value="/ListeAdmin" var="displayAll">
                            <c:param name="nav" value="displayAllPerson" />
                        </c:url>
                    <li><a href="${displayAll}">Afficher liste des personnes</a></li>
                    <li><a href="#">Afficher liste admin</a></li>
                    <li><a href="#">Afficher liste eseignant</a></li>
                    <li><a href="#">Afficher liste etudiant</a></li>
                    <li class="divider"></li>
                        <c:url value="/ListeAdmin" var="addAllUser">
                            <c:param name="nav" value="addUser" />
                        </c:url>
                    <li><a href="${addAllUser}">Ajouter utilisateur</a></li>
                        <c:url value="/ListeAdmin" var="displayUser">
                            <c:param name="nav" value="displayAllUser" />
                        </c:url>
                    <li><a href="${displayUser}">Afficher liste utilisateur</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gestion des promotions <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <c:url value="/ListeAdmin" var="displayPromotion">
                        <c:param name="nav" value="displayAllPromotion" />
                    </c:url>
                    <li><a href="${displayPromotion}">Afficher liste promotion</a></li>
                        <c:url value="/ListeAdmin" var="addPromotion">
                            <c:param name="nav" value="addPromotion" />
                        </c:url>
                    <li><a href="${addPromotion}">Ajouter promotion</a></li>
                </ul>
            </li>

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gestion des cours <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <c:url value="/ListeAdmin" var="displayMatiere">
                        <c:param name="nav" value="displayAllMatiere" />
                    </c:url>
                    <li><a href="${displayMatiere}">Afficher liste des matieres</a></li>
                    <li><a href="#">Ajouter matiere</a></li>
                    <li class="divider"></li>
                        <c:url value="/ListeAdmin" var="displayCours">
                            <c:param name="nav" value="displayAllCours" />
                        </c:url>
                    <li><a href="${displayCours}">Afficher liste des cours</a></li>
                    <li><a href="#">Ajouter cours</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gestion des absences <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <c:url value="/ListeAdmin" var="displayAbsence">
                        <c:param name="nav" value="displayAllAbsence" />
                    </c:url>
                    <li><a href="${listeAbsences}">Afficher liste des absences</a></li>
                    <li><a href="#">Ajouter absence</a></li>
                </ul>
            </li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
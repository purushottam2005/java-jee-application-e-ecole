<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix= "fmt" uri="http://java.sun.com/jstl/fmt" %>

<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.png">

        <title>site centre de formation</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/justified-nav.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">


    </head>

    <body>

        <div class="container">

            <div class="masthead">
                <!-- Menu navigation -->
                <jsp:include page="/WEB-INF/views/common/menuEnseignant.jsp" flush="true"/>
                <!-- Fin menu navigation -->
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="panel-heading"></div>
                      <div class="list-group">
                        <a href="#" class="list-group-item active">
                            FIL INFOS
                        </a>
                        <a href="#" class="list-group-item">Rentrée promotion : 25 septembre 2013</a>
                        <a href="#" class="list-group-item">Projet java JEE : 28 octobre 2013</a>
                        <a href="#" class="list-group-item">Projet Android : 26 décembre 2013</a>
                        <a href="#" class="list-group-item">.....</a>
                    </div>
                </div>

                <br />
                <div class="col-md-8">
                    <div class="panel panel-primary">
                          <div class="panel-heading">INFORMATIONS</div>

                        <ul class="list-group">
                            <li class="list-group-item">
                                <span class="badge">3</span>
                                Nombre total des étudiants
                            </li>
                            <li class="list-group-item">
                                <span class="badge">7</span>
                                Nombre total des matieres
                            </li>
                            <li class="list-group-item">
                                <span class="badge">12</span>
                                Nombre total des cours
                            </li>
                            <li class="list-group-item">
                                <span class="badge">2</span>
                                Nombre total des absences des étudiants
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!-- Site footer -->
        <div class="footer">
            <p></p>
        </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript"></script>
</body> 
</html>

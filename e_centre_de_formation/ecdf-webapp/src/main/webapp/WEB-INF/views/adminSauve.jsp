<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix= "fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:url var= "accueil" value="/HomeServlet" />
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.png">

        <title>site centre de formation</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/justified-nav.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">


    </head>

    <body>

        <div class="container">

            <div class="masthead">
                <h3 class="text-muted">ESPACE ADMINISTRATEUR</h3>
                <!-- Menu navigation -->
                <jsp:include page="/WEB-INF/views/common/menuAdmin.jsp" flush="true"/>
                <!-- Fin menu navigation -->
                <div class="panel-heading"></div>
            </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">PERSONNE : LISTE</div>
                    <div class="panel-body">

                        <table class="table">
                            <!-- Default panel contents -->
                            <thead>
                                <tr>
                                    <th>Role</th>
                                    <th>Nom</th>
                                    <th>Prenom</th>
                                    <th>e-mail</th>
                                    <th>Photo</th>
                                    <th>Adresse</th>
                                    <th>Cp</th>
                                    <th>Ville</th>
                                    <th>User</th>
                                    <th>Mdp</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${listeDePersonne}" var="nextPersonne">
                                    ${nextPersonne.value}
                                    <tr>
                                        <td>
                                            ${nextPersonne.value.role}
                                        </td>
                                        <td>
                                            ${nextPersonne.value.nom}
                                        </td>
                                        <td>
                                            ${nextPersonne.value.prenom}
                                        </td>
                                        <td>

                                        </td>
                                        <td>
                                            ${nextPersonne.value.photo}
                                        </td>
                                        <td>
                                            ${nextPersonne.value.adresse.rue}
                                        </td>
                                        <td>
                                            ${nextPersonne.value.adresse.codePostal}
                                        </td>
                                        <td>
                                            ${nextPersonne.value.adresse.ville}
                                        </td>
                                        <td>
                                            ${nextPersonne.value.loginInfos.login}
                                        </td>
                                        <td>
                                            ${nextPersonne.value.loginInfos.motDePasse}
                                        </td>
                                    </tr>
                                </c:forEach>   
                            </tbody>
                        </table>
                    </div>
                </div>

            <!-- Site footer -->
            <div class="footer">
                <p></p>
            </div>

        </div> <!-- /container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.js"></script>
    </body> 
</html>

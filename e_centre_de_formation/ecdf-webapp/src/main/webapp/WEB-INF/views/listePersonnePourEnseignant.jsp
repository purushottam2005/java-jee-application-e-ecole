<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix= "fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:url var= "accueil" value="/HomeServlet" />
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.png">

        <title>site centre de formation</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/justified-nav.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">


    </head>

    <body>

        <div class="container">

            <div class="masthead">
                <!-- Menu navigation -->
                <jsp:include page="/WEB-INF/views/common/menuEnseignant.jsp" flush="true"/>
                <!-- Fin menu navigation -->
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="panel-heading"></div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">PERSONNE : LISTE</div>
                        <div class="panel-body">

                            <table class="table">
                                <!-- Default panel contents -->
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Prenom</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${listeDePersonne}" var="nextPersonne"> 
                                        <tr>
                                            <td>
                                        ${nextPersonne.value.nom}
                                             
                                            </td>
                                            <td>
                                        ${nextPersonne.value.prenom}
                                             
                                            </td>
                                            <td>
                                                <c:url value="/CrudPersonne" var="accueilAlone" >
                                                    <c:param name="nav" value="displayAllPerson" />
                                                    <c:param name="nom" value="${nextPersonne.value.nom}" />
                                                    <c:param name="prenom" value="${nextPersonne.value.prenom}" />
                                                </c:url>

                                                <a class="avatar" href="${accueilAlone}" ><img src="images/editer.png" title="Voir informations"/></a>  
                                                
                                            </td>
                                         
                                        </tr>
                                    </c:forEach>   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <br />
                <div class="col-md-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading">INFORMATIONS</div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST">
                                <c:forEach items="${alonePersonne}" var="nextAlonePersonne">
                                <div class="form-group">
                                    <label for="inputText3"  class="col-sm-2 control-label">Nom</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="nomForm" class="col-xs-4" placeholder="Text input" value="${nextAlonePersonne.value.nom}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-sm-2 control-label">Prenom</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="prenomForm" class="col-xs-4" placeholder="Text input" value="${nextAlonePersonne.value.prenom}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <input type="date" id ="idDateNaissance" class="col-xs-4" placeholder="Text input">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-sm-2 control-label">Adresse</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="adresseForm" class="col-xs-4" placeholder="Text input" value="${nextAlonePersonne.value.adresse.rue}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-sm-2 control-label">Code postal</label>
                                    <div class="col-sm-10">
                                        <input type="text" id ="codePostalForm" class="col-xs-2" placeholder="Text input" value="${nextAlonePersonne.value.adresse.codePostal}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-sm-2 control-label" >Ville</label>
                                    <div class="col-sm-10">
                                        <input type="text" id ="villeForm" class="col-xs-3" placeholder="Text input" value="${nextAlonePersonne.value.adresse.ville}">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="col-xs-4"  placeholder="Text input" value="${nextAlonePersonne.value.email}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputText3" class="col-sm-2 control-label">Photo</label>
                                    <div class="col-sm-10">
                                        <img src="${nextAlonePersonne.value.photo}" id ="idPhoto" title="photo"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-sm-2 control-label">Utilisateur</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="col-xs-4" id ="idUtilisateur" placeholder="Text input" value="${nextAlonePersonne.value.loginInfos.login}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="col-xs-4" id="inputPassword3" placeholder="Password" value="${nextAlonePersonne.value.loginInfos.motDePasse}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputSelect" class="col-sm-2 control-label">Role</label>
                                    <div class="col-sm-10">
                                        <select class="col-xs-3">
                                            <option>Admin</option>
                                            <option>Enseignant</option>
                                            <option>Etudiant</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-10">
                                        <button type="submit" id="btnDelete" class="btn btn-success" name="btnDelete">Supprimer</button>
                                        <button type="submit" class="btn btn-danger" name="btnUpdate" >Modifier</button>
                                    </div>
                                </div>
                                </c:forEach>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Site footer -->
        <div class="footer">
            <p></p>
        </div>

    </div> <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript">
   // -------------------------------------
   function confirmDeletePersonne()
   // -------------------------------------
   {
      var reponse = window.confirm("Confirmez-vous la suppression de la personne?");
      if(reponse) window.location.href = "http://localhost:8080/CrudPersonne?";
     // if(reponse) window.location.href = "../controleurs/ControlSiteJr.php?action=InsererFacture&li_debut=<?php echo $liDebut;?>&userSalarie=<?php echo $_GET['userSalarie'];?>&typeSalarie=<?php echo $_GET['typeSalarie'];?>";

   }
   // ------------
   function init()
   // ------------
   {
      document.getElementById("btnDelete").onclick = confirmDeletePersonne;
   }

   window.onload = init;
</script>

    
</body> 
</html>

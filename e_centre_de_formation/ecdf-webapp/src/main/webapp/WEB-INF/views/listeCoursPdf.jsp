<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix= "fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:url var= "connexion" value="/Connexion" />
<html>
    <head>
        <title>Contenu cours PDF</title>
    </head>

    <body>
        <div class="container">
            <div class="masthead">
                <h3 class="text-muted"></h3>
            </div>
            <!-- Jumbotron -->
            <div class="jumbotron">
                <h1></h1>
            </div>
            <div class="container">
                <c:if test="${!empty param}">
                    <c:set var="cours" value="${param['cours']}"/>  
                <!-- Affiche les resultats dans un PDF --> 
                <object data="../resources/${cours}.pdf" width="1200" height="680" type="application/pdf" title="PDF"> 
                </object>
                </c:if>
            </div>
        </div>
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-lg-4">
            </div>
        </div>

        <!-- Site footer -->
        <div class="footer">
            <p></p>
        </div>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.js"></script>
</body> 
</html>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix= "fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:url var= "connexion" value="/Connexion" />
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.png">

        <title>site centre de formation</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/justified-nav.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="css/signin.css" rel="stylesheet">

    </head>

    <body>
        <div class="container">
            <div class="masthead">
                <h3 class="text-muted"></h3>
            </div>
            <!-- Jumbotron -->
            <div class="jumbotron">
                <h1></h1>
            </div>
            <div class="container">
                <form class="form-signin" method="post" action="${connexion}">
                    <div class="panel panel-primary">
                        <div class="panel-heading">IDENTIFICATION</div>
                        <div class="panel-body">
                            <input type="text" name="login" class="form-control" placeholder="login" >
                            <input type="password" name="password"class="form-control" placeholder="password">
                            <!--<label class="checkbox">
                              <input type="checkbox" value="remember-me"> Remember me
                            </label>-->
                            <button class="btn btn-warning" type="submit">Se connecter</button>
                        </div>
                    </div>
                <c:if test="${!empty param}">
                    <c:if test="${empty param['login']}">
                        <p class='alert-danger'>
                            <c:out  value="Veuillez saisir votre nom d'utilisateur !!" />
                        </p>
                    </c:if>
                    <c:if test="${empty param['password']}">
                        <p class='alert-danger'>
                            <c:out  value="Veuillez saisir votre mot de passe !!" />
                        </p>
                    </c:if>
                </c:if>
                </form>
            </div>
        </div>
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-lg-4">
            </div>
        </div>

        <!-- Site footer -->
        <div class="footer">
            <p></p>
        </div>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.js"></script>
</body> 
</html>
